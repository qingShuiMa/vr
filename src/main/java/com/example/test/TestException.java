package com.example.test;

import java.util.Scanner;

/**
 * @ClassName: TestException
 * @Author: zhoute
 * @Date: 2023-11-20
 **/

public class TestException {

    public static void main(String[] args) throws Exception {

        Scanner scanner = new Scanner(System.in);
        int i = scanner.nextInt();
        if (i > 0) {
            throw new RE(i + ">0");
        } else {
            throw new CE(i + "<0");
        }

    }

    static class RE extends RuntimeException {
        public RE(String msg) {
            super(msg);
        }
    }

    static class CE extends Exception {
        public CE(String msg) {
            super(msg);
        }
    }
}
