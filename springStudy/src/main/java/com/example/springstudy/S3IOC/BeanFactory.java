package com.example.springstudy.S3IOC;

public interface BeanFactory {


	Object getBean(String beanName);


	 void registerBeanDefinition(String beanName, BeanDefinition bean) throws InstantiationException, IllegalAccessException;

}
