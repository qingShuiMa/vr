package com.example.springstudy.S4IOC.factory;

import com.example.springstudy.S4IOC.beanDefinition.BeanDefinition;

public interface BeanFactory {


	Object getBean(String beanName);


	 void registerBeanDefinition(String beanName, BeanDefinition bean) throws InstantiationException, IllegalAccessException;

}
