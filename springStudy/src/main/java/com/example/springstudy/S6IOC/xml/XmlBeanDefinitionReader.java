package com.example.springstudy.S6IOC.xml;


import com.example.springstudy.S6IOC.beanDefinition.AbstractBeanDefinitionReader;
import com.example.springstudy.S6IOC.beanDefinition.BeanDefinition;
import com.example.springstudy.S6IOC.beanDefinition.BeanReference;
import com.example.springstudy.S6IOC.beanDefinition.Property;
import com.example.springstudy.S6IOC.io.ResourceLoader;
import org.junit.platform.commons.util.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.InputStream;

public class XmlBeanDefinitionReader extends AbstractBeanDefinitionReader {

	public XmlBeanDefinitionReader(ResourceLoader resourceLoader) {
		super(resourceLoader);
	}



	@Override
	public void loadBeanDefinitions(String location) throws Exception {
		InputStream inputStream = getResourceLoader().getResource(location).getInputStream();
		doLoadBeanDefinitions(inputStream);
	}

	protected void doLoadBeanDefinitions(InputStream inputStream) throws Exception {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = factory.newDocumentBuilder();
		Document doc = docBuilder.parse(inputStream);// 解析bean

		registerBeanDefinitions(doc);

		inputStream.close();
	}

	public void registerBeanDefinitions(Document doc) {
		Element root = doc.getDocumentElement();
		parseBeanDefinitions(root);
	}


	protected void parseBeanDefinitions(Element root) {
		NodeList nl = root.getChildNodes();
		for (int i = 0; i < nl.getLength(); i++) {
			Node node = nl.item(i);
			if (node instanceof Element) {
				Element ele = (Element) node;
				processBeanDefinition(ele);
			}

		}
	}

	private void processBeanDefinition(Element ele) {
		String name = ele.getAttribute("name");
		String beanClass = ele.getAttribute("class");
		BeanDefinition beanDefinition = new BeanDefinition();
		processProperty(ele, beanDefinition);
		try {
			beanDefinition.setBeanClass(Class.forName(beanClass));
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		getRegistry().put(name, beanDefinition);
	}


	private void processProperty(Element ele, BeanDefinition beanDefinition) {
		NodeList propertyNode = ele.getElementsByTagName("property");
		for (int i = 0; i < propertyNode.getLength(); i++) {
			Node node = propertyNode.item(i);
			if (node instanceof Element) {
				Element propertyEle = (Element) node;
				String name = propertyEle.getAttribute("name");
				String value = propertyEle.getAttribute("value");
				String ref = propertyEle.getAttribute("ref");

				if (!StringUtils.isBlank(value)) {
					// 处理直接注入值的情况
					beanDefinition.addPropertyValue(new Property(name, value));
				} else if (!StringUtils.isBlank(ref)) {
					// 处理依赖引用的情况，可以考虑使用懒加载
					// 先注册为BeanDefinition
					BeanReference beanReference = new BeanReference(ref);
					beanDefinition.addPropertyValue(new Property(name, beanReference));
				}
			}
		}
	}

}
