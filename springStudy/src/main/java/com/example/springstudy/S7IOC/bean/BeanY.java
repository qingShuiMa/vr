package com.example.springstudy.S7IOC.bean;

import lombok.Data;

@Data
public class BeanY {

	private String name;

	public void function() {
		System.out.println("我是Y");

	}
}
