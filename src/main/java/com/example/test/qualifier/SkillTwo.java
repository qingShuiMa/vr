package com.example.test.qualifier;

import org.springframework.stereotype.Component;

@Component
public class SkillTwo implements Skills {
	@Override
	public void run() {
		System.out.println("skillTwo+run");
	}

	@Override
	public void sing() {
		System.out.println("skillTwo+sing");
	}
}
