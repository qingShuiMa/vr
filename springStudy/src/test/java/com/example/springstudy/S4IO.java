package com.example.springstudy;

import com.example.springstudy.S4IOC.io.Resource;
import com.example.springstudy.S4IOC.io.ResourceLoader;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.io.InputStream;

@SpringBootTest
class S4IO {
	@Test
	void contextLoads() throws IOException {

		ResourceLoader resourceLoader = new ResourceLoader();
		Resource resource = resourceLoader.getResource("tinyioc.xml");
		InputStream inputStream = resource.getInputStream();
		Assertions.assertNotNull(inputStream);
	}
}
