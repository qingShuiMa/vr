package com.example.springstudy.S2IOC;


public interface BeanFactory {


	Object getBean(String beanName);


	 void registerBeanDefinition(String beanName, BeanDefinition bean);

}
