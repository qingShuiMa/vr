package com.example.test.valid;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class VV {
	@NotEmpty(message = "不能为空", groups = {A.class})
	private String name;
}
