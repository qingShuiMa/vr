package com.example.springstudy.S8IOC.bean;

import lombok.Data;

@Data
public class BeanY {

	private String name;

	public void function() {
		System.out.println("我是Y");

	}
}
