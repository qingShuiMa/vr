package com.example.selectPicFromPdf;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;

/**
 * @ClassName: SelectPicFromPdf
 * @Author: zhoute
 * @Date: 2023-11-13
 **/

public class onePdfToPictures {

    public static String PATH = "E:\\aoteman\\14口袋公园全套\\01.公园方案文本-173套\\2018-昆明新海河滨河带状公园工程项目（84页）.pdf";

    /**
     * 文件夹中的pdf
     *
     * @param args
     */
    public static void main(String[] args) {

        File file = new File(PATH);
        processPdfFile(file);

    }

    private static void processPdfFile(File pdfFile) {
        try {
            PDDocument document = PDDocument.load(pdfFile);

            // 创建一个与文档关联的PDFRenderer对象
            PDFRenderer pdfRenderer = new PDFRenderer(document);

            // 创建一个文件夹用于保存图片
            String outputFolderPath = pdfFile.getAbsolutePath().substring(0, pdfFile.getAbsolutePath().lastIndexOf('.'));
            File outputFolder = new File(outputFolderPath);
            if (!outputFolder.exists()) {
                outputFolder.mkdirs();
            }

            // 提取为图片
            int numPages = document.getNumberOfPages();

            for (int pageIndex = 0; pageIndex < numPages; pageIndex++) {
                // 渲染PDF页面为图像
                BufferedImage image = pdfRenderer.renderImageWithDPI(pageIndex, 150);

                // 保存图像到文件
                File outputImageFile = new File(outputFolder, "page" + (pageIndex + 1) + ".png");
                ImageIO.write(image, "png", outputImageFile);

                System.out.println("Image saved: " + outputImageFile.getAbsolutePath());
            }

            document.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
