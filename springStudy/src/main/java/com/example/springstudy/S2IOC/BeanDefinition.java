package com.example.springstudy.S2IOC;


import lombok.Data;

@Data
public class BeanDefinition {

	private Object bean;
	private Class beanClass;
	private String beanClassName;

	public BeanDefinition() {
	}
}


