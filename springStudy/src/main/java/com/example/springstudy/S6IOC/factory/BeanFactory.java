package com.example.springstudy.S6IOC.factory;

import com.example.springstudy.S6IOC.beanDefinition.BeanDefinition;

public interface BeanFactory {


	Object getBean(String beanName) throws InstantiationException, IllegalAccessException, Exception;


	 void registerBeanDefinition(String beanName, BeanDefinition bean) throws InstantiationException, IllegalAccessException;

}
