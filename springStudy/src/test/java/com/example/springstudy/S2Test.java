package com.example.springstudy;


import com.example.springstudy.S2IOC.AutowiredCapableBeanFactory;
import com.example.springstudy.S2IOC.BeanDefinition;
import com.example.springstudy.S2IOC.BeanDemo;
import com.example.springstudy.S2IOC.BeanFactory;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class S2Test {
	@Test
	void contextLoads() {

		System.out.println("Hello World!");
	}

	@Test
	void ioc() {

		//初始化一个具体的Bean工厂
		BeanFactory beanFactory = new AutowiredCapableBeanFactory();

		//注册Bean ,仍然是通过手动注入
		BeanDefinition beanDefinition = new BeanDefinition();
		try {
			beanDefinition.setBeanClass(Class.forName("com.example.springstudy.S2IOC.BeanDemo"));
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		beanFactory.registerBeanDefinition("BeanDemo", beanDefinition);

		//获取Bean
		BeanDemo beanDemo = (BeanDemo) beanFactory.getBean("BeanDemo");
		beanDemo.function();


	}

}
