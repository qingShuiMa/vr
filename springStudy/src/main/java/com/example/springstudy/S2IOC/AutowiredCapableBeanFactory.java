package com.example.springstudy.S2IOC;

public class AutowiredCapableBeanFactory extends AbstractBeanFactory {
	@Override
	protected Object doCreate(BeanDefinition beanDefinition) {

		try {
			return beanDefinition.getBeanClass().newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;

	}
}
