package com.example.test;

import com.example.VrApplication;
import org.apache.catalina.core.ApplicationContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @ClassName: q
 * @Author: zhoute
 * @Date: 2023-11-20
 **/
@Component
public class Q implements CommandLineRunner {

    @Autowired
    private T t1;


    private void kk() {
        System.out.println(t1.function());
    }

    public static void main(String[] args) throws Exception {

        ConfigurableApplicationContext run = SpringApplication.run(VrApplication.class, args);
        T t = new T() {
            @Override
            public String function() {
                return "name";
            }
        };

        Method function = t.getClass().getMethod("function");
        Object invoke = function.invoke(t);
        System.out.println(invoke.getClass());

//        System.out.println(run.getBeansOfType(T.class).values().stream().map(T::function).collect(Collectors.toList()));

    }

    @Override
    public void run(String... args) {
        kk();
    }
}
