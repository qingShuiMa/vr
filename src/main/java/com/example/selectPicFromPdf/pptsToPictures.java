package com.example.selectPicFromPdf;
import org.apache.poi.sl.usermodel.Slide;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xslf.usermodel.XSLFSlide;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * @ClassName: SelectPicFromPpt
 * @Author: zhoute
 * @Date: 2023-11-13
 **/

public class pptsToPictures {

    public static void main(String[] args) {
        String inputFolder = "path/to/your/ppt/folder"; // 替换为你的 PPT 文件夹路径
        File folder = new File(inputFolder);

        if (folder.exists() && folder.isDirectory()) {
            File[] pptFiles = folder.listFiles((dir, name) -> name.toLowerCase().endsWith(".pptx"));

            if (pptFiles != null && pptFiles.length > 0) {
                for (File pptFile : pptFiles) {
                    extractAndSaveImages(pptFile.getAbsolutePath());
                }
                System.out.println("图像提取成功，保存在相应的文件夹中。");
            } else {
                System.out.println("文件夹中没有找到 PPT 文件。");
            }
        } else {
            System.out.println("指定的路径不是一个文件夹。");
        }
    }

    private static void extractAndSaveImages(String inputPptPath) {
        String outputFolderPath = createOutputFolderPath(inputPptPath);

        try {
            extractImagesFromPPT(inputPptPath, outputFolderPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String createOutputFolderPath(String inputPptPath) {
        File file = new File(inputPptPath);
        String fileName = file.getName();
        String baseName = fileName.substring(0, fileName.lastIndexOf('.'));
        return file.getParent() + File.separator + baseName + "_Images";
    }

    private static void extractImagesFromPPT(String inputPptPath, String outputFolderPath) throws IOException {
        try (FileInputStream fis = new FileInputStream(inputPptPath);
             XMLSlideShow ppt = new XMLSlideShow(fis)) {

            File outputFolder = new File(outputFolderPath);
            if (!outputFolder.exists()) {
                outputFolder.mkdirs();
            }

            int imageNumber = 1;
            for (Slide slide : ppt.getSlides()) {

                BufferedImage image = new BufferedImage(slide.getSlideShow().getPageSize().width,
                        slide.getSlideShow().getPageSize().height, BufferedImage.TYPE_INT_RGB);
                XSLFSlide xslfSlide = (XSLFSlide) slide;
                xslfSlide.draw(image.createGraphics());

                String outputImagePath = outputFolderPath + File.separator + "Slide_" + imageNumber + ".png";
                ImageIO.write(image, "png", new File(outputImagePath));
                imageNumber++;
            }
        }
    }
}
