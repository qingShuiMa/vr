package com.example.selectPicFromPdf;

/**
 * @ClassName: picsToPdf
 * @Author: zhoute
 * @Date: 2023-11-13
 **/

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.graphics.image.LosslessFactory;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * 将文件夹中的图片合成一个pdf
 */
public class picsToPdf {

    public static void main(String[] args) {
        String inputFolderPath = "path/to/your/folder"; // 替换为你的图像文件夹路径
        String outputPdfPath = createOutputPdfPath(inputFolderPath);

        File inputFolder = new File(inputFolderPath);
        File[] imageFiles = inputFolder.listFiles((dir, name) -> name.toLowerCase().endsWith(".png"));

        if (imageFiles != null) {
            try {
                createPdfFromImages(imageFiles, outputPdfPath);
                System.out.println("PDF 创建成功: " + outputPdfPath);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("在指定的文件夹中未找到图像文件。");
        }
    }

    // 创建输出 PDF 文件的路径，使用输入文件夹的名称
    private static String createOutputPdfPath(String inputFolderPath) {
        File folder = new File(inputFolderPath);
        String folderName = folder.getName();
        return folder.getAbsolutePath() + File.separator + folderName + ".pdf";
    }

    // 从图像文件数组创建 PDF
    private static void createPdfFromImages(File[] imageFiles, String outputPdfPath) throws IOException {
        try (PDDocument document = new PDDocument()) {
            for (File imageFile : imageFiles) {
                BufferedImage image = ImageIO.read(imageFile);
                PDPage page = new PDPage();
                document.addPage(page);

                try (PDPageContentStream contentStream = new PDPageContentStream(document, page)) {
                    // 从 BufferedImage 创建 PDImageXObject
                    PDImageXObject pdImage = LosslessFactory.createFromImage(document, image);
                    // 在 PDF 页上绘制图像
                    contentStream.drawImage(pdImage, 0, 0, pdImage.getWidth(), pdImage.getHeight());
                }
            }

            // 保存 PDF 文件
            document.save(outputPdfPath);
        }
    }
}


