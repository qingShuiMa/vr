package com.example.springstudy.S5IOC.beanDefinition;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class PropertyValues {

	private List<Property> properties = new ArrayList<>();

}
