package com.example.codeGenerator;

import com.baomidou.mybatisplus.core.exceptions.MybatisPlusException;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.util.Scanner;

import static java.util.Collections.singletonMap;

/**
 * @ClassName: CodeGenerator
 * @Author: zhoute
 * @Date: 2023-11-14
 **/

public class CodeGenerator {
    static final String USER_DIR = System.getProperty("user.dir");
    static final String URL = "jdbc:mysql://10.74.1.16:3307/gdfs_bazx?characterEncoding=UTF-8&allowMultiQueries=true&serverTimezone=Asia/Shanghai&useSSL=false";
    static final String USERNAME = "root";
    static final String PASSWORD = "dsw6395530";
    static final String AUTHOR = "zhoute";


    public static void main(String[] args) {
        DataSourceConfig dataSourceConfig = new DataSourceConfig.Builder(URL, USERNAME, PASSWORD).build();
        AutoGenerator mpg = new AutoGenerator(dataSourceConfig);

// 全局配置
        GlobalConfig gc = new GlobalConfig.Builder()
                .outputDir(USER_DIR + "/src/main/java") //指定输出目录
                .author(AUTHOR)  // 设置作者名字
                .disableOpenDir()  //禁止打开输出目录	默认值:true
                .fileOverride()  //是否覆盖现在文件
                .enableSwagger()  //开启 swagger 模式	默认值:false
                .build();
        mpg.global(gc);

// 包配置
        PackageConfig pc = new PackageConfig.Builder()
                .parent("com.example.codeGenerator") //父包名
//                .moduleName("sys") //父包模块名	默认值:无
                .entity("entity") //Entity 包名	默认值:entity
//                .service("service") //Service 包名	默认值:service
//                .serviceImpl("service.impl") //Service Impl 包名	默认值:service.impl
//                .mapper("mapper") //Mapper 包名	默认值:mapper
//                .xml("mapper.xml") //Mapper XML 包名	默认值:mapper.xml
//                .controller("controller")//Controller 包名	默认值:controller
                .pathInfo(singletonMap(OutputFile.mapperXml, USER_DIR + "/src/main/resources/mapper/")) //路径配置信息
                .build();
        mpg.packageInfo(pc);

// 不生成
        TemplateConfig tc = new TemplateConfig.Builder()
//                .disable() //禁用所有模板
                .disable(TemplateType.ENTITY) //禁用单个模板	TemplateType.ENTITY
                .entity("/templates/entity.java")
                .service("/templates/service.java")
                .serviceImpl("/templates/serviceImpl.java")
                .mapper("/templates/mapper.java")
                .mapperXml("/templates/mapper.xml")
                .controller("/templates/controller.java")
                .build();
        mpg.template(tc);

// 策略配置
        StrategyConfig strategy = new StrategyConfig.Builder()
                .addFieldPrefix("t_", "mp_") // 要生成表的前缀，生成时会去掉前缀
                .addInclude(scanner("表名，多个英文逗号分割").split(",")) // 需要生成的表，不设置默认生成所有
                .entityBuilder() // 实体策略配置
                // .superClass(BaseDo.class)
                .addSuperEntityColumns("create_time", "create_user", "update_time", "update_user", "is_deleted")
                .enableLombok()
                .enableChainModel()
                .formatFileName("%sDo")
                .controllerBuilder() // controller 策略配置
                .enableRestStyle()
                .mapperBuilder() // Mapper 策略配置
                .enableBaseResultMap()   // SQL 映射文件
                .enableBaseColumnList()  // SQL 片段
                .build();
        mpg.strategy(strategy);

        mpg.execute(new FreemarkerTemplateEngine());
    }

    /**
     * 读取控制台内容
     */
    public static String scanner(String tip) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入" + tip + "：");
        if (scanner.hasNext()) {
            String ipt = scanner.next();
            if (StringUtils.isNotBlank(ipt)) {
                return ipt;
            }
        }
        throw new MybatisPlusException("请输入正确的" + tip + "！");
    }

}
