package com.example.springstudy;


import com.example.springstudy.S6IOC.context.ApplicationContext;
import com.example.springstudy.S6IOC.context.ClassPathXmlApplicationContext;
import com.example.springstudy.S7IOC.aop.AdvisedSupport;
import com.example.springstudy.S7IOC.aop.CustomMethodInterceptor;
import com.example.springstudy.S7IOC.aop.JdkDynamicAopProxy;
import com.example.springstudy.S7IOC.aop.TargetSource;
import com.example.springstudy.S7IOC.bean.BeanInterface;
import com.example.springstudy.S7IOC.bean.BeanInterfaceImpl;
import com.example.springstudy.S7IOC.bean.BeanX;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class S7Test {
	@Test
	void contextLoads() {

		System.out.println("Hello World!");
	}

	@Test
	void ioc() throws Exception {
		//可支持代理（通知）的对象
		AdvisedSupport advisedSupport = new AdvisedSupport();
		BeanInterface beanInterface = new BeanInterfaceImpl();
		//对原始对象进行包装
		TargetSource targetSource = new TargetSource(BeanInterface.class, beanInterface);
		advisedSupport.setTargetSource(targetSource);

		//包装一个方法拦截器（就是在原有方法基础上进行的方法增强）
		MethodInterceptor customMethodInterceptor = new CustomMethodInterceptor();
		advisedSupport.setMethodInterceptor(customMethodInterceptor);
		//创建代理对象
		JdkDynamicAopProxy jdkDynamicAopProxy = new JdkDynamicAopProxy(advisedSupport);
		BeanInterface proxy= (BeanInterface) jdkDynamicAopProxy.getProxy();

		//执行代理方法
		proxy.function();
	}

}
