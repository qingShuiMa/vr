S1
创建一个简单的bean工厂
提供 注册和 获取bean的功能

S2
考虑到bean的注册和获取的方式有多种
对bean工厂进行抽象

S3
给Bean加上属性

S4
通过XML配置Bean
-resource 包含一个url，以及从url建立IO连接
-resourceLoader 从指定位置一个resource
-接口：通过配置读取bean接口，唯一一个加载资源的方法
-抽象类：实现接口，有两个属性：a资源加载器 b存放beanDefinition的MAP
-XML资源bean加载器： xml配置读取接口，将XML 资源读取登记到抽象类中

S5
-对bean懒加载
-解决依赖循环的问题

S7
-AOP 动态代理的实现

S8
-切点表达式，精细化增强方法