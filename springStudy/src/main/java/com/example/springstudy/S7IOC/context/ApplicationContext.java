package com.example.springstudy.S7IOC.context;

import com.example.springstudy.S7IOC.factory.BeanFactory;

public interface ApplicationContext extends BeanFactory {
}
