package example.proxy;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @ClassName: ProxyParam
 * @Author: zhoute
 * @Date: 2023-11-25
 **/
@Data
@Component
public class ProxyParam {
    private String url;
    private String method;
    private Map<String, String> headers;
    private String body;

}
