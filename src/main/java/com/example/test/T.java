package com.example.test;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @ClassName: T1
 * @Author: zhoute
 * @Date: 2023-11-16
 **/
@Component
@Data
public class T {
    @Autowired
    private static String name = "T100";

    public String function() {
        return name;
    }


}
