package com.example.springstudy.S5IOC.beanDefinition;

/**
 * 从配置中读取BeanDefinition
 */
public interface BeanDefinitionReader {

	void loadBeanDefinitions(String location) throws Exception;
}
