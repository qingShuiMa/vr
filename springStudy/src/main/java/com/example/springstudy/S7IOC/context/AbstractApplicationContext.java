package com.example.springstudy.S7IOC.context;

import com.example.springstudy.S7IOC.factory.AbstractBeanFactory;

public abstract class AbstractApplicationContext implements ApplicationContext {

	protected AbstractBeanFactory abstractBeanFactory;

	public AbstractApplicationContext(AbstractBeanFactory abstractBeanFactory) {
		this.abstractBeanFactory = abstractBeanFactory;
	}

	public void refresh() throws Exception {
	}

	@Override
	public Object getBean(String name) throws Exception {
		return abstractBeanFactory.getBean(name);
	}
}
