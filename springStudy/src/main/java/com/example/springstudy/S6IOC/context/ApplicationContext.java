package com.example.springstudy.S6IOC.context;

import com.example.springstudy.S6IOC.factory.BeanFactory;

public interface ApplicationContext extends BeanFactory {
}
