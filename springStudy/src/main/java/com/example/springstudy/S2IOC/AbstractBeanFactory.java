package com.example.springstudy.S2IOC;

import java.util.HashMap;
import java.util.Map;

public abstract class AbstractBeanFactory implements BeanFactory {


	private Map<String, BeanDefinition> map = new HashMap<>();

	@Override
	public Object getBean(String beanName) {
		return map.get(beanName).getBean();
	}

	@Override

	public void registerBeanDefinition(String beanName, BeanDefinition beanDefinition) {
		//利用反射创建一个bean
		Object bean = doCreate(beanDefinition);
		beanDefinition.setBean(bean);
		map.put(beanName, beanDefinition);
	}

	/**
	 * 通过不同的工厂，装配bean
	 * @param beanDefinition
	 * @return
	 */
	protected abstract Object doCreate(BeanDefinition beanDefinition);


}
