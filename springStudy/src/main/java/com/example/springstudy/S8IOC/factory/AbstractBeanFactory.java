package com.example.springstudy.S8IOC.factory;

import com.example.springstudy.S8IOC.beanDefinition.BeanDefinition;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractBeanFactory implements BeanFactory {

	private Map<String, BeanDefinition> beanDefinitionMap = new HashMap<>();
	private List<String> beanDefinitionNames = new ArrayList<>();
	private Map<String, Object> earlyExposedBeans = new HashMap<>(); // 用于提前曝露的缓存

	@Override
	public Object getBean(String beanName) throws Exception {
		BeanDefinition beanDefinition = beanDefinitionMap.get(beanName);
		if (beanDefinition == null) {
			throw new IllegalArgumentException("No BeanDefinition :" + beanName);
		}

		// 先检查提前曝露的缓存
		Object earlyExposedBean = earlyExposedBeans.get(beanName);
		if (earlyExposedBean != null) {
			return earlyExposedBean;
		}

		Object bean = beanDefinition.getBean();
		if (bean == null) {
			bean = doCreateBean(beanDefinition);
			// 在创建完成后将其添加到提前曝露的缓存中
			earlyExposedBeans.put(beanName, bean);
		}
		return bean;
	}

	@Override
	public void registerBeanDefinition(String beanName, BeanDefinition beanDefinition) {
		beanDefinitionMap.put(beanName, beanDefinition);
		beanDefinitionNames.add(beanName);
	}
	//非懒加载的方法
	public void preInstantiateSingletons() throws Exception {
		for (String beanName : this.beanDefinitionNames) {
			getBean(beanName);
		}
	}

	protected abstract Object doCreateBean(BeanDefinition beanDefinition) throws Exception;
}

