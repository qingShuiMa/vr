package com.example.springstudy.S8IOC.io;

import java.io.IOException;
import java.io.InputStream;

/**
 * 内部资源定位的接口
 */
public interface Resource {

	InputStream getInputStream() throws IOException;
}
