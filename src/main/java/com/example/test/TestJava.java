package com.example.test;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @ClassName: TestJava
 * @Author: zhoute
 * @Date: 2023-11-20
 **/
@Data
public class TestJava {

/*    public static void main(String[] args) {

        // 1.1StringBuilder 类中构造器
        StringBuilder str1 = new StringBuilder("str1");
        StringBuilder str2 = new StringBuilder();
        // 1.2 StringBuilder 类中 append（）方法
        str2.append("str2");
        System.out.println(str1);
        System.out.println(str2);

        // 1.3 StringBuilder 类中 reverse() 方法
        StringBuilder str3 = str2.reverse();
        System.out.println(str3);
        // 1.4 StringBuilder 类中 length() 方法
        int length = str3.length();
        System.out.println(length);
        // 1.5 StringBuilder 类中 toString() 方法
        System.out.println(str1.toString());
        // 1.6 为什么操作字符串的建议使用 StringBuilder 类型 ，而不建议 String 类型？
        // 一个创建新对象，一个基于自身进行修改
        // 1.7 StringBuilder  与 StringBuffer 比较
        //Buffer是线程安全 Builder不是。


        Student s1 = new Student("张三", 3);
        Student s2 = new Student("李四", 2);
        Student s3 = new Student("王五", 1);
        List<Student> students = new ArrayList<>();
        Collections.addAll(students, s1, s2, s3);
        Collections.sort(students, Comparator.comparingInt(Student::getAge));
        for (Student student : students) {
            System.out.println(student.getAge());

        }





    }

    @Data
    @AllArgsConstructor
    static class Student {
        private String name;
        private int age;

    }*/


    public static void main(String[] args) {
        BigDecimal bigDecimal1 = new BigDecimal("1");
        BigDecimal bigDecimal2 = new BigDecimal("3");

        BigDecimal add = bigDecimal1.add(bigDecimal2);
        System.out.println(add);
    }


}

