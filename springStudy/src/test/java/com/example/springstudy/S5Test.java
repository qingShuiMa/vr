package com.example.springstudy;


import com.example.springstudy.S5IOC.bean.BeanX;
import com.example.springstudy.S5IOC.beanDefinition.BeanDefinition;
import com.example.springstudy.S5IOC.factory.AutowiredCapableBeanFactory;
import com.example.springstudy.S5IOC.io.ResourceLoader;
import com.example.springstudy.S5IOC.xml.XmlBeanDefinitionReader;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Map;

@SpringBootTest
class S5Test {
    @Test
    void contextLoads() {

        System.out.println("Hello World!");
    }

    @Test
    void ioc() throws Exception {

        //读取配置，并将bean配置转换成BeanDefinition登记在XML配置加载器中
        XmlBeanDefinitionReader xmlBeanDefinitionReader = new XmlBeanDefinitionReader(new ResourceLoader());
        xmlBeanDefinitionReader.loadBeanDefinitions("tinyReioc.xml");

        //注册Bean ,将BeanDefinition在BeanFactory中注册
        AutowiredCapableBeanFactory factory = new AutowiredCapableBeanFactory();
        for (Map.Entry<String, BeanDefinition> BeanDefinitionEntry : xmlBeanDefinitionReader.getRegistry().entrySet()) {
            factory.registerBeanDefinition(BeanDefinitionEntry.getKey(), BeanDefinitionEntry.getValue());
        }

        //获取Bean
        BeanX beanDemo = (BeanX) factory.getBean("beanX");
        System.out.println(beanDemo.getBeanY());
        beanDemo.function();


    }

}
