package com.example;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@EnableAspectJAutoProxy
@Slf4j
public class VrApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(VrApplication.class, args);
        log.info("我是log");
    }

}
