package com.example.springstudy.S8IOC.context;

import com.example.springstudy.S8IOC.factory.BeanFactory;

public interface ApplicationContext extends BeanFactory {
}
