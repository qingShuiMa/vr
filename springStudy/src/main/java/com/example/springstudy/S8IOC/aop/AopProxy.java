package com.example.springstudy.S8IOC.aop;

/**
 * 代理对象接口
 */
public interface AopProxy {

	public Object getProxy();

}
