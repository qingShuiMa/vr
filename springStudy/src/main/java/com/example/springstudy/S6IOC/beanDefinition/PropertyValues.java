package com.example.springstudy.S6IOC.beanDefinition;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class PropertyValues {

	private List<Property> properties = new ArrayList<>();

}
