package com.example.springstudy.S7IOC.aop;

import lombok.Data;

/**
 * 被代理对象的数据信息
 */
@Data
public class TargetSource {
	//类型
	private Class targetClass;
	//本身
	private Object target;

	public TargetSource(Class<?> targetClass, Object target) {
		this.targetClass = targetClass;
		this.target = target;
	}
}
