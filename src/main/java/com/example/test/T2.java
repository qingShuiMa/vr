package com.example.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

/**
 * @ClassName: T2
 * @Author: zhoute
 * @Date: 2023-11-16
 **/
@Component
public class T2 {
    //解决循环依赖，需要在类的属性字段上加上@Lazy，在类上添加无效。
    @Autowired
    private T1 t1;

}
