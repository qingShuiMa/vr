package com.example.springstudy.S7IOC.aop;

/**
 * 代理对象接口
 */
public interface AopProxy {

	public Object getProxy();

}
