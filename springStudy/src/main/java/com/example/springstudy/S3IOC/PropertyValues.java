package com.example.springstudy.S3IOC;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
@Data
public class PropertyValues {

	private List<Property> properties = new ArrayList<>();

}
