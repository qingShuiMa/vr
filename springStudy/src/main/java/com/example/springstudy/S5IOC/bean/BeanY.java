package com.example.springstudy.S5IOC.bean;

import lombok.Data;

@Data
public class BeanY {

	private BeanX beanX;

	public void function() {
	}
}
