package com.example.config;

import lombok.Data;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.List;
import java.util.stream.Collectors;

@RestControllerAdvice
public class GlobalExceptionHandler {

	@ExceptionHandler({BindException.class, MethodArgumentNotValidException.class})
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ResponseEntity<ExceptionResponse> handleValidationException(Exception ex) {
		BindingResult bindingResult;

		if (ex instanceof MethodArgumentNotValidException) {
			bindingResult = ((MethodArgumentNotValidException) ex).getBindingResult();
		} else {
			bindingResult = ((BindException) ex).getBindingResult();
		}

		List<String> errors = bindingResult.getAllErrors().stream()
				.map(this::resolveErrorMessage)
				.collect(Collectors.toList());

		return ResponseEntity.badRequest().body(new ExceptionResponse("Validation Failed", errors));
	}

	private String resolveErrorMessage(ObjectError error) {
		if (error instanceof FieldError) {
			return String.format("字段要求：%s，%s", ((FieldError) error).getField(), error.getDefaultMessage());
		}
		return error.getDefaultMessage();
	}

	// 定义一个用于封装异常信息的类
	@Data
	static class ExceptionResponse {
		private String message;
		private List<String> errors;

		public ExceptionResponse(String message, List<String> errors) {
			this.message = message;
			this.errors = errors;
		}

		// getter 和 setter 方法省略
	}
}
