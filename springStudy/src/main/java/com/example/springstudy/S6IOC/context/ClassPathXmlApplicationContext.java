package com.example.springstudy.S6IOC.context;

import com.example.springstudy.S6IOC.beanDefinition.BeanDefinition;
import com.example.springstudy.S6IOC.factory.AbstractBeanFactory;
import com.example.springstudy.S6IOC.factory.AutowiredCapableBeanFactory;
import com.example.springstudy.S6IOC.io.ResourceLoader;
import com.example.springstudy.S6IOC.xml.XmlBeanDefinitionReader;

import java.util.Map;

public class ClassPathXmlApplicationContext extends AbstractApplicationContext {

	private String configLocation;

	public ClassPathXmlApplicationContext(String configLocation) throws Exception {
		this(new AutowiredCapableBeanFactory(), configLocation);
	}

	public ClassPathXmlApplicationContext(AbstractBeanFactory beanFactory, String configLocation) throws Exception {
		super(beanFactory);
		this.configLocation = configLocation;
		refresh();
	}

	@Override
	public void registerBeanDefinition(String beanName, BeanDefinition bean) throws InstantiationException, IllegalAccessException {

	}
	//读取配置的时及把bean注册登记到容器
	@Override
	public void refresh() throws Exception {
		XmlBeanDefinitionReader xmlBeanDefinitionReader = new XmlBeanDefinitionReader(new ResourceLoader());
		xmlBeanDefinitionReader.loadBeanDefinitions(configLocation);
		for (Map.Entry<String, BeanDefinition> beanDefinitionEntry : xmlBeanDefinitionReader.getRegistry().entrySet()) {
			abstractBeanFactory.registerBeanDefinition(beanDefinitionEntry.getKey(), beanDefinitionEntry.getValue());
		}
	}


}
