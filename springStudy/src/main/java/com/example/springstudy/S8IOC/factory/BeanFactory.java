package com.example.springstudy.S8IOC.factory;

import com.example.springstudy.S8IOC.beanDefinition.BeanDefinition;

public interface BeanFactory {


	Object getBean(String beanName) throws InstantiationException, IllegalAccessException, Exception;


	 void registerBeanDefinition(String beanName, BeanDefinition bean) throws InstantiationException, IllegalAccessException;

}
