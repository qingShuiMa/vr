package com.example.springstudy.S7IOC.aop;

import lombok.Data;
import org.aopalliance.intercept.MethodInterceptor;

/**
 * 代理相关的元数据
 * 可支持通知，即可支持
 */
@Data
public class AdvisedSupport {

	//代理对象包装信息
	private TargetSource targetSource;

	//方法拦截器 方法级别的拦截器
	private MethodInterceptor methodInterceptor;


}
