package com.example.springstudy.S3IOC;

import lombok.Data;

import java.util.List;


@Data
public class BeanDefinition {

	private Object bean;
	private Class beanClass;
	private String beanClassName;
	private PropertyValues propertyValues;

	public BeanDefinition() {
	}

	public void addPropertyValue(Property property) {
		List<Property> properties = new PropertyValues().getProperties();
		if (!properties.contains(property)) {
			properties.add(property);
		}
		this.propertyValues = new PropertyValues();
		propertyValues.setProperties(properties);
	}

}


