package com.example.springstudy.S1IOC;

import java.util.HashMap;
import java.util.Map;

public class BeanFactory {


	private Map<String, BeanDefinition> map = new HashMap<>();

	public Object getBean(String beanName) {
		return map.get(beanName).getBean();
	}


	public void registerBeanDefinition(String beanName, BeanDefinition bean) {
		map.put(beanName, bean);
	}

}
