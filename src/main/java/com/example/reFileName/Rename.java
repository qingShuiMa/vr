package com.example.reFileName;

import java.io.File;
import java.io.FilenameFilter;

/**
 * 根据后缀，批量修改文件名称
 */
public class Rename {
	public static String PATH = "E:\\BaiduNetdiskDownload\\奥特曼\\574中式茶叶店\\574中式茶叶店";
	public static String NEWNAME = "中式茶叶店";

	public static void main(String[] args) {
		File folder = new File(PATH);

		// 检查目录是否存在
		if (!folder.exists() || !folder.isDirectory()) {
			System.out.println("指定的路径不是一个有效的目录。");
			return;
		}

		// 使用文件过滤条件
		File[] skpFiles = folder.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.toLowerCase().endsWith(".skp");
			}
		});

		// 检查文件数组是否为null
		if (skpFiles != null) {
			for (int i = 0; i < skpFiles.length; i++) {
				File file = skpFiles[i];

				// 文件存在性检查
				if (file.isFile() && file.exists()) {
					String nowName = file.getName();
					int index = nowName.lastIndexOf(".");
					String extension = "";
					String baseName = nowName;

					// 检查是否有后缀
					if (index != -1) {
						baseName = nowName.substring(0, index);
						extension = nowName.substring(index);
					}

					// 索引逻辑，确保不为负数
					int newIndex = i + 1;
					if (newIndex < 1) {
						newIndex = 1;
					}

					String newName = NEWNAME + newIndex + extension;

					// 文件重名检查，避免覆盖
					File dest = new File(folder, newName);
					int counter = 1;
					while (dest.exists()) {
						newName = NEWNAME + newIndex + "_duplicate_" + counter + extension;
						dest = new File(folder, newName);
						counter++;
					}

					// 重命名操作
					if (file.renameTo(dest)) {
						System.out.println("文件重命名成功: " + dest.getAbsolutePath());
					} else {
						System.out.println("文件重命名失败: " + file.getAbsolutePath());
					}
				}
			}
		} else {
			System.out.println("无法读取目录下的文件。");
		}
	}
}
