package com.example.springstudy;

import com.example.springstudy.S3IOC.*;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class S3Test {
	@Test
	void contextLoads() {

		System.out.println("Hello World!");
	}

	@Test
	void ioc() throws InstantiationException, IllegalAccessException {

		//初始化一个具体的Bean工厂
		BeanFactory beanFactory = new AutowiredCapableBeanFactory();

		//注册Bean ,仍然是通过手动注入
		BeanDefinition beanDefinition = new BeanDefinition();
		try {
			beanDefinition.setBeanClass(Class.forName("com.example.springstudy.S3IOC.BeanDemo"));
			beanDefinition.addPropertyValue(new Property("name","我的name是name"));
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		beanFactory.registerBeanDefinition("BeanDemo", beanDefinition);

		//获取Bean
		BeanDemo beanDemo = (BeanDemo) beanFactory.getBean("BeanDemo");
		System.out.println(beanDemo.getName());
		beanDemo.function();


	}

}
