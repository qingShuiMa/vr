package com.example.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * @ClassName: Aspect
 * @Author: zhoute
 * @Date: 2023-11-25
 **/
//可以根据切面要实现的功能来定义多个不同的切面
@Aspect
@Component
public class AspectLog {


    //每个切面可以定义多个切点
    @Pointcut("execution(* com.example..*.*(..))")
    public void all() {
    }

    /**
     * @param joinPoint 封装了代理方法信息的对象,若用不到则可以忽略不写
     *                  前置方法
     */
    @Before("all()")
    public void all(JoinPoint joinPoint) {
        System.out.println("AOP，before方法被调用");
        System.out.println(joinPoint.getSignature());
    }
}
