package com.example.selectPicFromPdf;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.springframework.scheduling.annotation.Async;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @ClassName: SelectPicFromPdf
 * @Author: zhoute
 * @Date: 2023-11-13
 **/

public class pdfsToPictures {

    public static String PATH = "E:\\aoteman\\14口袋公园全套\\01.公园方案文本-173套";

    /**
     * 文件夹中的pdf
     * @param args
     */
    public static void main(String[] args) {

            File folder = new File(PATH);
            File[] pdfFiles = folder.listFiles((dir, name) -> name.toLowerCase().endsWith(".pdf"));

            if (pdfFiles != null && pdfFiles.length > 0) {
                // 创建一个线程池
                int numThreads = Runtime.getRuntime().availableProcessors();
                ExecutorService executorService = Executors.newFixedThreadPool(numThreads);

                // 处理每个PDF文件
                Arrays.asList(pdfFiles).forEach(pdfFile -> {
                    executorService.submit(() -> processPdfFile(pdfFile));
                });

                // 关闭线程池
                executorService.shutdown();
            } else {
                System.out.println("No PDF files found in the specified folder.");
            }
        }

        private static void processPdfFile(File pdfFile) {
            try {
                PDDocument document = PDDocument.load(pdfFile);

                // 创建一个与文档关联的PDFRenderer对象
                PDFRenderer pdfRenderer = new PDFRenderer(document);

                // 创建一个文件夹用于保存图片
                String outputFolderPath = pdfFile.getAbsolutePath().substring(0, pdfFile.getAbsolutePath().lastIndexOf('.'));
                File outputFolder = new File(outputFolderPath);
                if (!outputFolder.exists()) {
                    outputFolder.mkdirs();
                }

                // 提取为图片
                int numPages = document.getNumberOfPages();

                for (int pageIndex = 0; pageIndex < numPages; pageIndex++) {
                    // 渲染PDF页面为图像
                    BufferedImage image = pdfRenderer.renderImageWithDPI(pageIndex, 150);

                    // 保存图像到文件
                    File outputImageFile = new File(outputFolder, "page" + (pageIndex + 1) + ".png");
                    ImageIO.write(image, "png", outputImageFile);

                    System.out.println("Image saved: " + outputImageFile.getAbsolutePath());
                }

                document.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


}
