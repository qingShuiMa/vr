package com.example.springstudy.S4IOC.factory;

import com.example.springstudy.S4IOC.beanDefinition.BeanDefinition;

import java.util.HashMap;
import java.util.Map;

public abstract class AbstractBeanFactory implements BeanFactory {


	private Map<String, BeanDefinition> map = new HashMap<>();

	@Override
	public Object getBean(String beanName) {
		return map.get(beanName).getBean();
	}

	@Override
	public void registerBeanDefinition(String beanName, BeanDefinition beanDefinition) throws InstantiationException, IllegalAccessException {
		//利用beanDefinition中的className,反射创建一个bean
		Object bean = doCreateBean(beanDefinition);
		beanDefinition.setBean(bean);
		map.put(beanName, beanDefinition);
	}

	/**
	 * 通过不同的工厂，装配bean
	 * @param beanDefinition
	 * @return
	 */
	protected abstract Object doCreateBean(BeanDefinition beanDefinition) throws InstantiationException, IllegalAccessException;


}
