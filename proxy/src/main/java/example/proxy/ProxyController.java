package example.proxy;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.Objects;

/**
 * @ClassName: CodeGenerator
 * @Author: zhoute
 * @Date: 2023-11-14
 **/
@RestController("")
@Slf4j
public class ProxyController {

    @Autowired
    private RestTemplate restTemplate;


    @GetMapping("proxy")
    public AjaxResult getServerInfo(HttpServletRequest request) {
        try {
            // 获取服务器名称和端口号
            String serverName = request.getServerName();
            int serverPort = request.getServerPort();

            // 获取服务器的主机地址
            String hostAddress = InetAddress.getLocalHost().getHostAddress();

            // 构建服务器信息字符串
            String info = String.format("服务器名称: %s, 服务器端口: %d, 主机地址: %s", serverName, serverPort, hostAddress);

            // 记录信息日志
            log.info("成功获取服务器信息: {}", info);

            // 返回包含服务器信息的AjaxResult对象
            return AjaxResult.success(info);
        } catch (UnknownHostException e) {
            // 记录错误日志
            log.error("获取服务器信息失败: {}", e.getMessage(), e);
            return AjaxResult.error("获取服务器信息失败");
        }
    }
    //请求格式示例
/*
    {
        "url":"https://api.example.com/resource",
        "method":"POST",
        "headers":{
           "Content-Type":"application/json",
            "Authorization":"Bearer yourAccessToken",
            "app_id":"Mu6Z6CLp",
            "app_secret":"a99593e66571a84a14536facf97697908c435e6e"
        },
        "body":{
            "user":{
                "username":"john_doe",
                "email":"john.doe@example.com"
            },
            "products": [
                {
                    "id":"123",
                    "name":"Product A",
                    "price":29.99
                },
                {
                    "id":"456",
                    "name":"Product B",
                    "price":19.99
                }
           ],
            "settings":{
                "theme":"dark",
                "notifications":true
            }
        }
    }

*/


    @PostMapping("proxy")
    public AjaxResult proxyRequest(@RequestBody ProxyParam proxyParam) {
        try {
            // 校验必要参数不为空
            validateProxyParam(proxyParam);

            // 从ProxyParam对象中获取请求参数
            String url = proxyParam.getUrl();
            url = url.replaceFirst("https", "http");
            String method = proxyParam.getMethod().toUpperCase();
            Map<String, String> headers = proxyParam.getHeaders();
            String body = proxyParam.getBody();
            log.info("收到代理请求 - URL: {}, 方法: {}, 头部: {}, 主体: {}", url, method, headers, body);

            // 创建HTTP请求头
            HttpHeaders httpHeaders = new HttpHeaders();
            if (headers != null) {
                httpHeaders.setAll(headers);
            }

            // 创建HTTP请求实体
            HttpEntity<String> requestEntity = new HttpEntity<>(body, httpHeaders);

            // 转发请求到目标服务器
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, Objects.requireNonNull(HttpMethod.resolve(method)), requestEntity, String.class);

            // 记录信息日志
            log.info("请求转发成功，目标URL: {}, 响应: {}", url, responseEntity.getBody());

            // 返回目标服务器的响应
            return AjaxResult.success("请求转发成功", responseEntity.getBody());
        } catch (Exception e) {
            // 记录错误日志
            log.error("请求转发失败: {}", e.getMessage(), e);
            return AjaxResult.error("请求转发失败," + e.getMessage());
        }
    }

    // 校验 ProxyParam
    private void validateProxyParam(ProxyParam proxyParam) {
        try {
            // 使用 Assert 工具类校验 ProxyParam 对象不为 null
            Assert.notNull(proxyParam, "ProxyParam对象不能为null");

            // 使用 StringUtils 工具类校验字符串不为空
            Assert.isTrue(StringUtils.isNotBlank(proxyParam.getUrl()), "URL不能为空");
            Assert.isTrue(StringUtils.isNotBlank(proxyParam.getMethod()), "Method不能为空");

            // 如果校验通过，记录校验成功的信息日志
            log.info("ProxyParam校验成功，URL: {}, Method: {}", proxyParam.getUrl(), proxyParam.getMethod());
        } catch (IllegalArgumentException e) {
            // 如果校验失败，记录错误信息日志，并抛出异常
            log.error("ProxyParam校验失败: {}", e.getMessage(), e);
            throw new IllegalArgumentException("ProxyParam校验失败: " + e.getMessage(), e);
        }
    }

}
