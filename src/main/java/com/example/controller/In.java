package com.example.controller;

import com.example.test.qualifier.Skills;
import com.example.test.valid.A;
import com.example.test.valid.B;
import com.example.test.valid.VV;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.apiguardian.api.API;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.Objects;

@RestController
@Slf4j
@Tag(name = "测试")
public class In {

	@Qualifier("skillOne")
	@Resource
	@Autowired
	private Skills skills;

	//	public In(@Qualifier("skillOne") Skills skills) {
//		this.skills = skills;
//	}
	@Operation(summary = "A方法")
	@PostMapping("/A")
	public String A(@Validated(A.class) @RequestBody VV vv) {
//		if (bindingResult.hasFieldErrors()) {
//			log.info(Objects.requireNonNull(bindingResult.getFieldError()).getDefaultMessage());
//			bindingResult.getAllErrors().stream().peek(p -> {
//				System.out.println(p.getDefaultMessage());
//			});
//			return "字段错误";
//		}
		return vv.getName();
	}

	@PostMapping("/B")
	public String B(@Validated(B.class) @RequestBody VV vv, BindingResult bindingResult) {
		if (bindingResult.hasFieldErrors()) {
			log.info(Objects.requireNonNull(bindingResult.getFieldError()).getDefaultMessage());
			bindingResult.getAllErrors().stream().peek(p -> {
				System.out.println(p.getDefaultMessage());
			});
			return "字段错误";
		}
		return vv.getName();
	}
}
