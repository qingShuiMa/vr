package com.example.springstudy.S8IOC.beanDefinition;

import lombok.Data;

@Data
public class BeanReference extends BeanDefinition {
	private String BeanReferenceName;

	public BeanReference(String name) {
		this.BeanReferenceName = name;
	}


}
