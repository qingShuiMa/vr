package com.example.test.qualifier;

import org.springframework.stereotype.Component;

@Component
public class SkillOne implements Skills {
	@Override
	public void run() {
		System.out.println("skillOne+run");
	}

	@Override
	public void sing() {
		System.out.println("skillOne+sing");
	}
}
