package com.example.springstudy.S5IOC.beanDefinition;

import lombok.Data;

@Data
public class Property {

	private String name;
	private Object value;

	public Property(String name, Object value) {
		this.name = name;
		this.value = value;
	}
}
