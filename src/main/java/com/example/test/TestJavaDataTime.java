package com.example.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * @ClassName: TestJavaDataTime
 * @Author: zhoute
 * @Date: 2023-11-20
 **/

public class TestJavaDataTime {
    public static void main(String[] args) {
        //日期格式化  DateTimeFormatter
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        System.out.println(now.format(dtf));
        System.out.println(dtf.format(now));


        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String a = "1233-12-22 12:11:43";
        Date date = new Date();
        String format1 = format.format(date);
        try {
            System.out.println(format.format(format.parse(a)));
        } catch (ParseException ignored) {

        }
        System.out.println(date);
        System.out.println(format1);

    }
}
