package com.example.springstudy.S8IOC.factory;

import com.example.springstudy.S8IOC.beanDefinition.BeanDefinition;
import com.example.springstudy.S8IOC.beanDefinition.BeanReference;
import com.example.springstudy.S8IOC.beanDefinition.Property;

import java.lang.reflect.Field;

public class AutowiredCapableBeanFactory extends AbstractBeanFactory {
	@Override
	protected Object doCreateBean(BeanDefinition beanDefinition) throws Exception {
		//创建一个Bean
		Object bean = createBeanInstance(beanDefinition);
		//加一级缓存 否侧内存溢出
		beanDefinition.setBean(bean);
		//注册Bean属性
		applyPropertyValues(bean, beanDefinition);
		return bean;

	}

	private Object createBeanInstance(BeanDefinition beanDefinition) throws Exception {
		return beanDefinition.getBeanClass().newInstance();
	}

	private void applyPropertyValues(Object bean, BeanDefinition beanDefinition) throws Exception {
		for (Property property : beanDefinition.getPropertyValues().getProperties()) {
			Field declaredField = bean.getClass().getDeclaredField(property.getName());
			declaredField.setAccessible(true);

			// 添加引用类型的属性
			Object value = property.getValue();
			if (value instanceof BeanReference) {
				BeanReference beanReference = (BeanReference) value;
				value = getBean(beanReference.getBeanReferenceName());
			}

			declaredField.set(bean, value);
		}
	}


}
