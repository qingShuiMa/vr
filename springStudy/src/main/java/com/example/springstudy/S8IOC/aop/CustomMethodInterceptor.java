package com.example.springstudy.S8IOC.aop;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

public class CustomMethodInterceptor implements MethodInterceptor {
	@Override
	public Object invoke(MethodInvocation invocation) throws Throwable {
		System.out.println("我拦截了你的方法");
		Object proceed = invocation.proceed();
		System.out.println("你的方法执行完了");
		return proceed;
	}
}
