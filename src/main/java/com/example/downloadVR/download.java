package com.example.downloadVR;

import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @ClassName: download
 * @Author: zhoute
 * @Date: 2023-11-08
 **/
@RestController
@RequestMapping("")
@Tag(name = "下载VR")
public class download {

	private final RestTemplate restTemplate;


	public download(RestTemplateBuilder restTemplateBuilder) {
		this.restTemplate = restTemplateBuilder.build();
	}

	@PostMapping("/aa")
	public void downloadImage(@RequestBody Qaa qaa) {
		char[] ch = {'r', 'l', 'u', 'd', 'b', 'f'};
		String imageUrl = qaa.getUrl().replace("512x", "4999x"); // 替换为图像的 URL
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(MediaType.IMAGE_JPEG)); // 设置接受图像类型为 JPEG
		HttpEntity<String> entity = new HttpEntity<>(headers);

		ExecutorService executor = Executors.newFixedThreadPool(6);

		for (int i = 0; i < 6; i++) {
			char ch1 = ch[i];
			int i1 = imageUrl.lastIndexOf('_');
			imageUrl = imageUrl.substring(0, i1 + 1) + ch1 + imageUrl.substring(i1 + 2);

			String finalImageUrl = imageUrl;
			executor.submit(() -> {
				ResponseEntity<byte[]> response = restTemplate.exchange(finalImageUrl, HttpMethod.GET, entity, byte[].class);

				if (response.getStatusCode() == HttpStatus.OK && response.getHeaders().getContentType() != null &&
						response.getHeaders().getContentType().includes(MediaType.IMAGE_JPEG)) {
					String substring = "";
					try {
						int questionMarkIndex = finalImageUrl.indexOf('?'); // 找到问号 ? 的索引
						if (questionMarkIndex >= 0) {
							int lastSlashIndex = finalImageUrl.lastIndexOf('/', questionMarkIndex); // 在问号 ? 前寻找最近一个斜杠 /
							if (lastSlashIndex >= 0) {
								// 提取问号 ? 前最近一个斜杠 / 后的内容作为文件名
								substring = finalImageUrl.substring(lastSlashIndex + 1, questionMarkIndex);
							}
						}

						File folder = new File("阳新VR", qaa.getFileName()); // 获取路径
						if (!folder.exists() && !folder.mkdirs()) {
							System.out.println("Failed to create folder: " + folder.getAbsolutePath());
							return;
						}

						File file = new File(folder, substring); // 获取路径
						if (!file.exists()) {
							if (!file.createNewFile()) {
								System.out.println("Failed to create file: " + file.getAbsolutePath());
								return;
							}
						}

						try (FileOutputStream fos = new FileOutputStream(file)) {
							fos.write(response.getBody());
							System.out.println("Image downloaded and saved at: " + file);
						} catch (IOException e) {
							e.printStackTrace();
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				} else {
					System.out.println("The response content type is not an image or download failed.");
				}
			});
		}

		executor.shutdown();
	}


}
