package com.example.springstudy.S4IOC.beanDefinition;

import lombok.Data;

@Data
public class Property {

	private String name;
	private String value;

	public Property(String name, String value) {
		this.name = name;
		this.value = value;
	}
}
