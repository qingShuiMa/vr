package com.example.springstudy.S8IOC.bean;

public class BeanInterfaceImpl implements BeanInterface {

	@Override
	public void function() {
		System.out.println("我是接口的实现类");
	}
}
