package com.example.springstudy.S3IOC;

import lombok.Data;

@Data
public class BeanDemo {

	private String name;

	public void function() {
		System.out.println("我是一个Bean");
	}
}
