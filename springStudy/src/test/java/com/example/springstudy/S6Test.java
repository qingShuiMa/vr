package com.example.springstudy;


import com.example.springstudy.S6IOC.bean.BeanX;
import com.example.springstudy.S6IOC.context.ApplicationContext;
import com.example.springstudy.S6IOC.context.ClassPathXmlApplicationContext;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class S6Test {
	@Test
	void contextLoads() {

		System.out.println("Hello World!");
	}

	@Test
	void ioc() throws Exception {

		//读取配置+注册bean
		ApplicationContext context = new ClassPathXmlApplicationContext("tinyReioc.xml");

		//获取Bean
		BeanX beanDemo = (BeanX) context.getBean("beanX");
		System.out.println(beanDemo.getName());


	}

}
