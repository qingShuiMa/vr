package com.example.springstudy.S4IOC.factory;

import com.example.springstudy.S4IOC.beanDefinition.BeanDefinition;
import com.example.springstudy.S4IOC.beanDefinition.Property;

import java.lang.reflect.Field;

public class AutowiredCapableBeanFactory extends AbstractBeanFactory {
	@Override
	protected Object doCreateBean(BeanDefinition beanDefinition) throws InstantiationException, IllegalAccessException {
		//创建一个Bean
		Object bean = createBeanInstance(beanDefinition);
		//注册Bean属性
		createBeanProperties(bean, beanDefinition);
		return bean;

	}

	private Object createBeanInstance(BeanDefinition beanDefinition) throws InstantiationException, IllegalAccessException {
		return beanDefinition.getBeanClass().newInstance();
	}

	private void createBeanProperties(Object bean, BeanDefinition beanDefinition) {
		for (Property property : beanDefinition.getPropertyValues().getProperties()) {
			Field declaredField = null;
			try {
				//getDeclaredField 获取所有字段  getfield 获取公开字段
				declaredField = bean.getClass().getDeclaredField(property.getName());
				declaredField.setAccessible(true);
				declaredField.set(bean, property.getValue());
			} catch (NoSuchFieldException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}

	}
}
