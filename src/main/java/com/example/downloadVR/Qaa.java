package com.example.downloadVR;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @ClassName: Qaa
 * @Author: zhoute
 * @Date: 2023-11-08
 **/
@Data
@Schema(name = "Qaa类")
public class Qaa {
	@Schema(name = "url")
	private String url;
	@Schema(name = " 文件名称")
	private String fileName;

}
