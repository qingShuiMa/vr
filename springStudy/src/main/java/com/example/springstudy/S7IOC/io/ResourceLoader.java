package com.example.springstudy.S7IOC.io;

import java.net.URL;

/**
 * 从指定的location获取资源
 */
public class ResourceLoader {

	public Resource getResource(String location) {
		//读取资源
		URL resource = this.getClass().getClassLoader().getResource(location);
		//封装成resource
		return new UrlResource(resource);
	}

}
