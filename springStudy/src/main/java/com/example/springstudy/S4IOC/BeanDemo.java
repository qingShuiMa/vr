package com.example.springstudy.S4IOC;

import lombok.Data;

@Data
public class BeanDemo {

	private String name;

	public void function() {
		System.out.println("我是一个Bean");
	}
}
